package MainAndBank;

public class BankAmountController {
	
	private static BankAmountController instance;
	private double amount;
	
	private BankAmountController(){}
	public static BankAmountController getInstance(){
		if (instance == null)
			instance = new BankAmountController();
		return instance;
	}
	public void addAmount(double amount){
		this.amount += amount;
	}
	public void substractAmount(double amount){
		this.amount -= amount;
	}
	public double getAmount(){
		return amount;
	}
}
