package MainAndBank;
import java.util.ArrayList;

import Account.Account;
import Operations.Income;
import Operations.Outcome;
import Operations.Transfer;


public class Bank {

	private ArrayList<Account> accounts = new ArrayList<Account>();
	private BankAmountController moneyController = BankAmountController.getInstance();
	
	public void income(Account inAccount, double amount)
	{
		Income income = new Income(inAccount, amount);
		inAccount.doOperation(income);
	}
	public void outcome(Account inAccount, double amount)
	{
		Outcome outcome = new Outcome(inAccount, amount);
		inAccount.doOperation(outcome);
		
	}
	public void transfer(Account fromAccount, Account toAccount, double amount)
	{
		Transfer transfer = new Transfer(fromAccount, toAccount, amount);
		fromAccount.doOperation(transfer);
	}
	public ArrayList<Account> getAccounts() {
		return accounts;
	}
	public void addAccount(Account account)
	{
		accounts.add(account);
	}
	public void deleteAccount(Account account)
	{
		accounts.remove(account);
	}
	public double getOverAllAmount() {
		return moneyController.getAmount();
	}
	public String toString()
	{
		return "Liczba kont w banku " + accounts.size() + "  || ilosc pieniedzy w banku " + moneyController.getAmount() + " PLN";
	}
}
