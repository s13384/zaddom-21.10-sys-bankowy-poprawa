package Operations;
import Account.Account;
import Account.HistoryLog;


public class Transfer extends Operation {

	Account fromAccount;
	Account toAccount;
	double amount;
	
	public Transfer(Account fromAccount , Account toAccount, double amount)
	{
		this.fromAccount = fromAccount;
		this.toAccount = toAccount;
		this.amount = amount;
	}
	@Override
	public void execute() {
		
		if(fromAccount.getAmount()>=amount){
			fromAccount.substract(amount);
			toAccount.add(amount);
			addLog(fromAccount, OperationType.outcome, toAccount.getName());
			addLog(toAccount, OperationType.income, fromAccount.getName());
		}	
		toString();
	}
	private void addLog(Account account, OperationType type, String name)
	{
		account.addLog(new HistoryLog(" ", type, amount, name));
	}
	public String toString(){
		if (fromAccount.getAmount()>=amount)
		System.out.println("BankingSystem : " + fromAccount.getName() + "->" + fromAccount.getNumber() +
				" przekazuje " +
				toAccount.getName() + "->" + toAccount.getNumber() + " " + amount + " PLN");
	
		else	
		System.out.println("BankingSystem : " + fromAccount.getName() + "->" + fromAccount.getNumber() +
						" niema wystarczajaco duzo srodkow by przekazac " +
						toAccount.getName() + "->" + toAccount.getNumber() + " " + amount + " PLN");
		return  toStringInfo;
	}

}
