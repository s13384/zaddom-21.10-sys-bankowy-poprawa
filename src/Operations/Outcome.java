package Operations;
import Account.Account;
import Account.HistoryLog;


public class Outcome extends Operation {

	Account account;
	double amount;
	
	public Outcome(Account account, double amount)
	{
		this.account = account;
		this.amount = amount;
	}
	@Override
	public void execute() {
		if (account.getAmount()>=amount){
			account.substract(amount);
			addLog(account);
			bankAmountController.substractAmount(amount);
		}
		toString();
	}
	private void addLog(Account account)
	{
		account.addLog(new HistoryLog(" ", OperationType.outcome, amount));
	}
	public String toString(){
		if (account.getAmount()>=amount)
		System.out.println("BankingSystem : " + account.getName() + "->" + account.getNumber() +
				" wyplaca " + amount + " PLN");
		else
			System.out.println("BankingSystem : " + account.getName() + "->" + account.getNumber() +
					" nie ma wystajczajaca duzo srodkow by wyplacic " + amount + " PLN");
			return toStringInfo;
	}
}
