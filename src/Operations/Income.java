package Operations;
import Account.Account;
import Account.HistoryLog;


public class Income extends Operation{

	Account account;
	double amount;
	
	public Income(Account account, double amount)
	{
		this.account = account;
		this.amount = amount;
	}
	@Override
	public void execute() {
		
		account.add(amount);
		addLog(account);
		bankAmountController.addAmount(amount);
		
		toString();
	}
	private void addLog(Account account)
	{
		account.addLog(new HistoryLog(" ", OperationType.income, amount));
	}
	public String toString(){
		System.out.println("BankingSystem : " + account.getName() + "->" + account.getNumber() +
				" otrzymuje " + amount + " PLN");
		return toStringInfo;
	}
	

}
