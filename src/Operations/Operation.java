package Operations;
import MainAndBank.BankAmountController;


public abstract class Operation {

	String name;
	final String toStringInfo = "This is console print function";
	protected static BankAmountController bankAmountController;
	
	public abstract void execute();
	public static void setBankAmountController(BankAmountController bankAmountController){
		Operation.bankAmountController = bankAmountController;
	}
	
}
