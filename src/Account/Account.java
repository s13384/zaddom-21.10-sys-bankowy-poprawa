package Account;
import Operations.Operation;


public class Account {

	private static int autoNumber = 0;
	
	private String number;
	private String name;
	private History history;
	private double amount;
	
	public Account(String name)
	{
		this.name = name;
		this.number = Integer.toString(autoNumber);
		this.history = new History();
		this.amount = 0;
		
		autoNumber++;
	}
	public String getNumber() {
		return number;
	}
	public History getHistory() {
		return history;
	}

	public double getAmount() {
		return amount;
	}
	public void add(double amount)
	{
		this.amount+=amount;
	}
	public void substract (double amount){
		this.amount-=amount;
	}
	public void doOperation(Operation operation){
		operation.execute();
	}
	public String getName() {

		return name;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void addLog(HistoryLog log)
	{
		history.addLog(log);
	}
	public String toString()
	{
			return name + " Nr " + number + " Posiada " + amount + " PLN";
	}
	public void printHistory()
	{
		System.out.println("-- Historia Konta " + name + " --");
		history.printHistory();
		System.out.println();
	}
	
	
}
